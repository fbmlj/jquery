$(document).ready(function () {
  var status = true
  $("img").on('click', function () {
    if (status) {
      $("img").animate({
        width: "+=200",
        height: "+=200"
      }, 500, function () { });
      status = false;
    }
    else {
      $("img").animate({

        width: "-=200",
        height: "-=200"
      }, 500, function () { });
      status = true;
    }

  })
})

